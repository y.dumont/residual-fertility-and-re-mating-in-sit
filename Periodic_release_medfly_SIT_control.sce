// residual fertility and remating SIT
// Yves DUMONT
// CIRAD & UP 
// August 2023
//
// This code is not "optimal" in terms of computational time: it could be reduced with a more awkward algorithm. However, it is sufficient to illustrate the main question of our manuscript.
//
function yp = desyd(t,y)
    global params
//
 epsilon=params(1);
 r=params(2);
 b=params(3);
 K=params(4);
 mu_S=params(5);
 mu_M=params(6);
 mu_F=params(7);
 nu_A=params(8);
 mu_A=params(9);
 nu_Y=params(10);
 delta=params(11);
 mu_Y=params(12);
 gama=params(13);
 deltaS=params(14);
//
//
 yp = [0,0,0,0,0,0];
 yp(1) =  b*(y(4)+epsilon*y(5))*(1-y(1)/K)-(nu_A+mu_A)*y(1);
 yp(2) =  r*nu_A*y(1)+delta*y(4)+deltaS*y(5)-(nu_Y+mu_Y)*y(2);
 yp(3) =  (1-r)*nu_A*y(1)-mu_M*y(3);
 yp(4)= nu_Y*y(3)/(y(3)+gama*y(6))*y(2)-(delta+mu_F)*y(4);
 yp(5)= nu_Y*gama*y(6)/(y(3)+gama*y(6))*y(2)-(deltaS+mu_F)*y(5);
 yp(6)= -mu_S*y(6);
endfunction
//
global params
//
GRO=0; // WITH or WITHOUT GRO Treatment
lim=5;  // limit size
Lambda_choice=10000 // chosen threshold value for the release size; chosen by the reader
tdebut=100; // starting time of the SIT treatment
//
tau=7 // periodicity of the releases
// 
// name of the file where to record the results
 savefile="SIT_nectarine_crit_release_rate_calculation_tau_"+string(tau)+"GRO"+string(GRO)+".txt"
// 
N=20;  // Number of point for the residual fertility

//
// WITHOUT GRO TREATMENT: values are calculated according to the explanations given in the manuscript
if GRO==0
    epsilon_tab(1,:)=linspace(0,0.0068,N);
    epsilon_tab(2,:)=linspace(0,0.0078,N);
    epsilon_tab(3,:)=linspace(0,0.0089,N);
//    
    delta_tab=[0,0.16,0.16];
    deltaS_tab=[0,0.16,0.3161];
    gama=0.38/(1-0.38);
else
// WITH GRO TREATMENT
    epsilon_tab(1,:)=linspace(0,0.0068,N);
    epsilon_tab(2,:)=linspace(0,0.0074,N);
    epsilon_tab(3,:)=linspace(0,0.0084,N);    
//
    delta_tab=[0,0.1,0.1];
    deltaS_tab=[0,0.1,0.2361];
    gama=0.67/(1-0.67);
end
//
// choice of the final time: the larger the residual fertility the larger the final time needed 
// to reach, eventually, equilibrium 0. 
//
 tfinaltab(1,:)=10000*ones(1,N);
 tfinaltab(1,N-2)=20000;
 tfinaltab(1,N-1)=40000;
tfinaltab(1,N)=60000;
//
 tfinaltab(2,:)=10000*ones(1,N);
 tfinaltab(2,N-4)=20000;
 tfinaltab(2,N-3)=20000;
 tfinaltab(2,N-2)=40000;
 tfinaltab(2,N-1)=40000;
tfinaltab(2,N)=60000;
//
 tfinaltab(3,:)=10000*ones(1,N);
 tfinaltab(3,N-4)=20000;
 tfinaltab(3,N-3)=20000;
 tfinaltab(3,N-2)=40000;
 tfinaltab(3,N-1)=40000;
tfinaltab(3,N)=60000;
//
// biological parameters
 r = 0.53;
 b= 12.135;  
 mu_S=log(2)/3;
 mu_M= 1/50.33; 
 mu_F= 1/42.66; 
 nu_A=0.027;
 mu_A=0.0227;
 nu_Y=1;
//
 mu_Y=mu_F;
 K = 10000;
//
for jj=1:3
    delta=delta_tab(jj)
    deltaS=deltaS_tab(jj)
    BoN=b*r*nu_A*nu_Y/(nu_A+mu_A)/(mu_Y*delta+mu_F*(nu_Y+mu_Y))
    BoNS=b*r*nu_A*nu_Y/(nu_A+mu_A)/(mu_Y*deltaS+mu_F*(nu_Y+mu_Y))
    RoN=b*r*nu_A*nu_Y/(nu_A+mu_A)/mu_F/(nu_Y+mu_Y)
    Lambda_S=Lambda_choice;
    for kk=1:N
// calculation for each residual fertility value
            Lambda_S_min=Lambda_S;
             Lambda_S_max=500*Lambda_S;
//       
        clear t sol
        tfinal=tfinaltab(jj,kk)
        epsilon=epsilon_tab(jj,kk)
        // Equilibrium without control
        A0_star=(1-1/BoN)*K;
        Y0_star=r*nu_A*(delta+mu_F)/(nu_Y*mu_F+mu_Y*(delta+mu_F))*A0_star;
        F0W_star=r*nu_A*nu_Y/(nu_Y*mu_F+mu_Y*(delta+mu_F))*A0_star;
        M0_star=(1-r)*nu_A/mu_M*A0_star;
//
     test=0;Lambda_S=0;
 while (abs(Lambda_S_max-Lambda_S_min)>lim) 
     // iteration loop to find the critical release value \Lambda_crit using an iterative approach
    Lambda_S=(Lambda_S_max+Lambda_S_min)/2
    params=[epsilon,r,b,K,mu_S,mu_M,mu_F,nu_A,mu_A,nu_Y,delta,mu_Y,gama,deltaS];
//    
if tdebut>0
    t=0:1:tdebut
    sol = ode("rkf",[A0_star;Y0_star;M0_star;F0W_star;0;0],0,t,desyd);
else
    sol=[A0_star;Y0_star;M0_star;F0W_star;0;0];
    t=0;
end
// resolution of the system between two consecutive releases
    Nper=round((tfinal-tdebut)/tau);
 for i=1:Nper
     endy=length(t);
     A_init=sol(1,endy);
     Y_init=sol(2,endy);
     M_init=sol(3,endy);
     FW_init=sol(4,endy);
     FS_init=sol(5,endy);
     MS_init=sol(6,endy)+tau*Lambda_S;
     t1=tdebut+(i-1)*tau:1:tdebut+i*tau;
     initials=[A_init;Y_init;M_init;FW_init;FS_init;MS_init];
     sol1 = ode("rkf",initials,tdebut+(i-1)*tau,t1,desyd);
     endy=length(t);
     sol=[sol(:,1:endy-1) sol1];
     t=[t(1:endy-1) t1];
 end
// computations until the final time
 if Nper*tau+tdebut<tfinal
         endy=length(t1);
     A_init=sol1(1,endy);
     Y_init=sol1(2,endy);
     M_init=sol1(3,endy);
     FW_init=sol1(4,endy);
     FS_init=sol1(5,endy);
     MS_init=sol1(6,endy);
     initials=[A_init;Y_init;M_init;FW_init;FS_init;MS_init];
     t1=tdebut+i*tau:1:tfinal
     sol1 = ode("rk",initials,tdebut+(i-1)*tau,t1,desyd);
     endy=length(t);
     sol=[sol(:,1:endy-1) sol1];
     t=[t(1:endy-1) t1];
 end
 endy=length(t);
 test=sum(sol(1:4,endy));
     if test<lim
         Lambda_S_max=Lambda_S;
     else
         Lambda_S_min=Lambda_S;
      end
 end
 resultat_taux_lacher(jj,kk)=tau*Lambda_S
    end
end
//
// record results in the savefile
//
    fid=file("open",savefile);
    write(fid,M0_star);
    write(fid,epsilon_tab)
    write(fid,resultat_taux_lacher)
    file("close",fid);
//
// end of the code 
