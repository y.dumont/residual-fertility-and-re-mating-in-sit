# Estimate the critical rate for Periodic releases when residual fertility and re-mating occur in Sterile Insect Technique Program against fruit flies

This is a computational code to estimate the critical rate for periodic releases to reach elimination when residual fertility occurs, and, also, in the case where no remating occurs, equal re-mating, and non-equal re-mating occurs within a medfly population.

The code is written in Scilab (freely available here: https://www.scilab.org/). At the end of the computation, the results are recorded in a given txt-file that can be used to plot the results. Depending on the computer, the computations can take several hours.

Output txt files are also available for the reader's convenience.

Figures 3 and 4 in the manuscript have been made using these txt files.

***

This Scilab code has been developed by **Yves Dumont** within the framework of the paper 
"**On the impact of re-mating and residual fertility on the Sterile Insect Technique efficacy: case study with the medfly Ceratitis capitata"**, 
authored by **Yves Dumont** and **Clélia Oliva**.


The preprint is available here:
https://www.biorxiv.org/content/10.1101/2023.08.17.552275v1.abstract
